"use strict";
        var generateDepartmentList = new Array("Ain", "Aisne", "Allier", "Alpes-de-Haute-Provence", "Alpes-Maritimes", "Ardèche", "Ardennes", "Ariège", "Aube", "Aude", "Aveyron", "Bas-Rhin", "Bouches-du-Rhône", "Calvados", "Cantal", "Charente", "Charente-Maritime", "Cher", "Corrèze", "Corse-du-Sud", "Côte-d'Or", "Côtes-d'Armor", "Creuse", "Deux-Sèvres", "Dordogne", "Doubs", "Drôme", "Essonne", "Eure", "Eure-et-Loir", "Finistère", "Gard", "Gers", "Gironde", "Guadeloupe", "Guyane", "Haut-Rhin", "Haute-Corse", "Haute-Garonne", "Haute-Loire", "Haute-Marne", "Haute-Saône", "Haute-Savoie", "Haute-Vienne", "Hautes-Alpes", "Hautes-Pyrénées", "Hauts-de-Seine", "Hérault", "Ille-et-Vilaine", "Indre", "Indre-et-Loire", "Isère", "Jura", "La Réunion", "Landes", "Loir-et-Cher", "Loire", "Loire-Atlantique", "Loiret", "Lot", "Lot-et-Garonne", "Lozère", "Maine-et-Loire", "Manche", "Marne", "Martinique", "Mayenne", "Mayotte", "Meurthe-et-Moselle", "Meuse", "Morbihan", "Moselle", "Nièvre", "Nord", "Oise", "Orne", "Paris", "Pas-de-Calais", "Puy-de-Dôme", "Pyrénées-Atlantiques", "Pyrénées-Orientales", "Rhône", "Saône-et-Loire", "Sarthe", "Savoie", "Seine-et-Marne", "Seine-Maritime", "Seine-Saint-Denis", "Somme", "Tarn", "Tarn-et-Garonne", "Territoire de Belfort", "Val-d'Oise", "Val-de-Marne", "Var", "Vaucluse", "Vendée", "Vienne", "Vosges", "Yonne", "Yvelines");
        var d = document.departmentListForm.departmentList;
        for (var i = 0; i < generateDepartmentList.length; i++) {
d.length++;
        d.options[d.length - 1].text = generateDepartmentList[i];
}

function getRegion(department){
var department = document.getElementById("choix").value;
        alert(department);
        if (department == "Ain" || department == "Allier" || department == "Ardèche" || department == "Cantal" || department == "Drôme" || department == "Isère" || department == "Loire" || department == "Haute-Loire" || department == "Puy-de-Dôme" || department == "Rhône" || department == "Savoie" || department == "Haute-Savoie") {
document.getElementById("resultat").innerHTML = "Auvergne-Rhône-Alpes";
        }
else if (department == "Côte-d'Or" || department == "Doubs" || department == "Jura" || department == "Nièvre" || department == "Haute-Saône" || department == "Saône-et-Loire" || department == "Yonne" || department == "Territoire de Belfort") {
document.getElementById("resultat").innerHTML = "Bourgogne-Franche-Comté";
        }
else if (department == "Cher" || department == "Eure-et-Loire" || department == "Indre" || department == "Indre-et-Loire" || department == "Loir-et-Cher" || department == "Loiret") {
document.getElementById("resultat").innerHTML = "Centre-Val de Loire";
        }
else if (department == "Côtes-d'Armor" || department == "Finistère" || department == "Ille-et-Vilaine" || department == "Morbihan") {
document.getElementById("resultat").innerHTML = "Bretagne";
        }
else if (department == "Corse-du-Sud" || department == "Haute-Corse") {
document.getElementById("resultat").innerHTML = "Corse";
        }
else if (department == "Ardennes" || department == "Aube" || department == "Marne" || department == "Haute-Marne" || department == "Meurthe-et-Moselle" || department == "Meuse" || department == "Moselle" || department == "Bas-Rhin" || department == "Haut-Rhin" || department == "Vosges") {
document.getElementById("resultat").innerHTML = "Grand Est";
        }
else if (department == "Aisne" || department == "Nord" || department == "Oise" || department == "Pas-de-Calais" || department == "Somme") {
document.getElementById("resultat").innerHTML = "Hauts-de-France";
        }
else {
document.getElementById("resultat").innerHTML = "";
        }

//var generateRegion = new Array("Auvergne-Rhône-Alpes", "Bourgogne-Franche-Comté", "Bretagne", "Centre-Val de Loire", "Corse", "Grand Est", "Hauts-de-France", "Île-de-France", "Normandie", "Nouvelle-Aquitaine", "Occitanie", "Pays de la Loire", "Provence-Alpes-Côtes d'Azur");
}