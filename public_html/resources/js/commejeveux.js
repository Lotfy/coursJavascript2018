/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function knowAge() {

    var age = document.getElementById("age").value;
    var masculin = document.getElementById("masculin");
    var feminin = document.getElementById("feminin");

    if (age < 0) {
        document.getElementById('resultat').innerHTML = "Vous n'êtes pas encore né";
    } else {
        if (age >= 0 && age < 2) {
            document.getElementById('resultat').innerHTML = "Vous êtes un bébé";
        } else if (age >= 2 && age < 7 && masculin.checked) {
            document.getElementById('resultat').innerHTML = "Vous êtes un jeune enfant";
        } else if (age >= 2 && age < 7 && feminin.checked) {
            document.getElementById('resultat').innerHTML = "Vous êtes une jeune fille";
        } else if (age >= 7 && age < 12 && masculin.checked) {
            document.getElementById('resultat').innerHTML = "Vous êtes un enfant qui atteint l'âge de raison";
        } else if (age >= 7 && age < 12 && feminin.checked) {
            document.getElementById('resultat').innerHTML = "Vous êtes une enfant qui atteint l'âge de raison";
        } else if (age >= 12 && age < 18 && masculin.checked) {
            document.getElementById('resultat').innerHTML = "Vous êtes un adolescent";
        } else if (age >= 12 && age < 18 && feminin.checked) {
            document.getElementById('resultat').innerHTML = "Vous êtes une adolescente";
        } else if (age >= 18 && age < 60 && masculin.checked) {
            document.getElementById('resultat').innerHTML = "Vous êtes un adulte";
        } else if (age >= 18 && age < 60 && feminin.checked) {
            document.getElementById('resultat').innerHTML = "Vous êtes une adulte";
        } else if (age >= 60 && masculin.checked) {
            document.getElementById('resultat').innerHTML = "Vous êtes un senior";
        } else if (age >= 60 && feminin.checked) {
            document.getElementById('resultat').innerHTML = "Vous êtes une senior";
        } else {
            document.getElementById('resultat').innerHTML = "Vous n'avez pas saisi de sexe!";
        }
    }
}


var tableau = ["Damien", "Lotfy", "Gael", "Fanny", "Stephanie", "Elisabeth", "Kevin", "Jeremy", "Mehdi", "Marie"]
tableau.forEach(function (element) {
    console.log(element);
});

for (var element in tableau) {
    console.log(tableau[element]);
}


var tab = ["wallay", 1, 4, 5, "brra", 8];
var add = "";
var somme = 0;
var firstindex = 0;

for (var i = 0; i < tab.length; i++) {
    if (!isNaN(tab[i])) {
        if (firstindex === 0) {
            add += "&nbsp&nbsp&nbsp" + tab[i] + "<br/>";
            somme += tab[i];
            firstindex++;
        } else {
            add += "+ " + tab[i] + "<br/>";
            somme += tab[i];
        }
    }
}
document.getElementById('add').innerHTML = add;
document.getElementById("result").innerHTML = somme;